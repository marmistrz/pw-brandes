#ifndef GENERATE_GRAPH_H
#define GENERATE_GRAPH_H

#include "graph.h"

/**
 * @brief generates an Erdos-Renyi binomial graph
 * @param vertices number of vertices
 * @param p probability of an edge between two vertices
 */
Graph ERGraph(int vertices, double p, bool directed);

/**
 * @brief Generates a Barabassi-Albert network
 * @param start_vertices size of the initial clique
 * @param vertices target size of a clique
 * @param new_conn number of connections for a newly created vertex
 */
Graph BAGraph(int start_vertices, int vertices, int new_conn);


#endif // GENERATE_GRAPH_H
