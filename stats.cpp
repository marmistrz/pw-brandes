#include "stats.h"

#include <numeric>
#include <algorithm>
#include <cassert>

namespace stats
{
    T average(data v)
    {
        long double zero = 0;
        T sum = std::accumulate(v.begin(), v.end(), zero);
        return sum / v.size();
    }

    static T square(T v)
    {
        return v * v;
    }

    T variance(data v)
    {
        long double zero = 0;
        T avg = average(v);
        T res = std::accumulate(v.begin(), v.end(), zero, [avg] (T res, T el) {
            return res + square(el - avg);
        });
        return res / v.size();
    }

    T mean(data v)
    {
        assert(std::is_sorted(v.begin(), v.end()));
        size_t last = v.size() - 1;
        size_t meanind = last / 2;
        if (last % 2 == 0) {
            return v[meanind];
        } else {
            return (v[meanind] + v[meanind+1])/2;
        }
    }

    std::vector<distr_interval> distribution(data v, uint16_t intervals)
    {
        assert(std::is_sorted(v.begin(), v.end()));
        std::vector<distr_interval> out(intervals);
        T min = *v.begin();
        T max = *std::prev(v.end());
        T span = max - min;
        T int_span = span / intervals;

        auto loit = v.begin();
        for (size_t i = 0; i < intervals; ++i) {
            T lo = min + i * int_span;
            T hi = min + (i+1) * int_span;
            auto hiit = std::lower_bound(loit, v.cend(), hi);
            size_t num = std::distance(loit, hiit);
            out[i] = {
                .min = lo,
                .max = hi,
                .count = num
            };
            loit = hiit;
        }
        // fix rounding errors
        out[intervals - 1].count += std::distance(loit, v.end());
        out[intervals - 1].max = max;
        return out;
    }
}

