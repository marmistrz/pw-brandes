#ifndef GRAPH_H
#define GRAPH_H

#include <map>
#include <set>
#include <vector>
#include <string>
#include <unordered_map>

class Graph
{
public:
    std::vector<std::vector<int>> edges;

    Graph() = default;
    Graph(int num_vertices);
    Graph(const std::vector<std::pair<int, int>>& edges);

    void add_edge(int src, int dest);
    void add_edge_undirected(int src, int dest);

    int size() const
    {
        return edges.size();
    }
};

#endif // GRAPH_H
