#ifndef STATS_H
#define STATS_H

#include <vector>
#include <cstdint>
#include <cstddef>

namespace stats
{
    using T = double; // NOTE we'll make templates when we need it :)
    using data = const std::vector<T>&;

    struct distr_interval {
        T min, max;
        uint64_t count;
    };

    T average(data v);
    T variance(data v);
    T mean(data v);
    std::vector<distr_interval> distribution(data v, uint16_t intervals);
}

#endif // STATS_H
