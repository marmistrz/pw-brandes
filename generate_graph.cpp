#include "generate_graph.h"

#include <random>

bool new_edge(double p)
{
    // FIXME this may be not the best way of seeding the random engine
    // but it's better than not seeding it at all
    static thread_local std::default_random_engine e{std::random_device()()};
    std::uniform_real_distribution<double> random(0, 1);
    return random(e) <= p;
}

Graph ERGraph(int vertices, double p, bool directed)
{
    Graph g(vertices);
    if (directed) {
        #pragma omp parallel for
        for (int i = 0; i < vertices; ++i) {
            for (int j = 0; j < vertices; ++j) {
                if (i != j && new_edge(p)) {
                    g.add_edge(i, j); // we only access edges[i] in a single thread
                }
            }
        }
    } else {
        #pragma omp parallel for
        for (int i = 0; i < vertices; ++i) {
            for (int j = 0; j < i; ++j) {
                if (new_edge(p)) {
                    g.add_edge(i, j);
                }
            }
        }

        for (int i = 0; i < vertices; ++i) {
            for (int j : g.edges[i]) {
                g.add_edge(j, i);
            }
        }
    }

    return g;
}

static std::pair<Graph, std::vector<int>> gen_clique(int n, int N)
{
    Graph g(N);
    std::vector<int> distr;
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < i; ++j) {
            g.add_edge(i, j);
            distr.push_back(i);
            distr.push_back(j);
        }
    }
    return {g, distr};
}

Graph BAGraph(int start_vertices, int vertices, int new_conn)
{
    Graph g;
    std::vector<int> distr;
    std::tie(g, distr) = gen_clique(start_vertices, vertices);

    static std::default_random_engine e{std::random_device()()};

    for (int i = start_vertices; i < vertices; ++i) {
        std::uniform_int_distribution<uint64_t> random(0, distr.size() - 1);

        for (int c = 0; c < new_conn; ++c) {
            int v = distr[random(e)];
            g.add_edge(i, v);
            distr.push_back(i);
            distr.push_back(v);
        }
    }

    return g;
}
