#include <cassert>
#include <string>
#include <iostream>
#include <fstream>
#include <functional>
#include <vector>
#include <chrono>
#include <unordered_set>
#include <array>
#include <algorithm>
#include <cstring>

#include <libgen.h>
#include <getopt.h>

#include "graph.h"
#include "brandes.h"
#include "generate_graph.h"
#include "stats.h"

using time_point = std::chrono::high_resolution_clock::time_point;

enum class Mode {
    File,
    Binomial,
    Evolving,
    Undefined
};

Mode mode = Mode::Undefined;
struct {
    int n = -1; // total num of vertices
    double p = -1; // ER probability
    int directed = -1;
    int N = -1; // number of starting vertices, only for BA
    int nconnections = -1; // number of connections in each evolution, only for BA
} genparams;

std::string infile, outprefix;

std::vector<std::pair<int, int>> get_edges(const std::string& file)
{
    std::vector<std::pair<int, int>> res;
    int x, y;
    std::ifstream f(file);
    if (!f) {
        perror("Error while opening file\n");
        exit(1);
    }
    while (f >> x >> y) {
        res.push_back(std::make_pair(x, y));
    }
    if (!f.eof()) {
        std::cerr << "There was an error processing the file, aborting\n";
        exit(1);
    }
    return res;
}

void write_param(int gsize, double param, std::string file)
{
    std::ofstream out(file);
    out.precision(std::numeric_limits<double>::max_digits10);
    if (!out) {
        perror("Error opening output file");
        exit(1);
    }
    out << gsize << " " << param << "\n";
}

void write_distr(int gsize, const std::vector<stats::distr_interval>& vec,
                 std::string file)
{
    std::ofstream out(file);
    out.precision(std::numeric_limits<double>::max_digits10);
    if (!out) {
        perror("Error opening output file");
        exit(1);
    }
    out << gsize << "\n\n";
    for (auto it = vec.begin(); it != vec.end(); ++it) {
        out << it->min << " " << it->max << " " << it->count << "\n";
    }
}

void write_results(std::vector<double>& results, const Graph& graph)
{
    int n = graph.size();
    std::ofstream out(outprefix + "results.txt");
    if (!out) {
        perror("Error opening output file");
        exit(1);
    }
    out.precision(std::numeric_limits<double>::max_digits10);
    out << n << "\n\n";
    for (size_t i = 0; i < results.size(); ++i) {
        if (!graph.edges[i].empty()) {
            out << i << " " << results[i] << "\n";
        }
    }
    out.close();

    std::sort(results.begin(), results.end());
    write_param(n, stats::mean(results), outprefix + "mean.txt");
    write_param(n, stats::average(results), outprefix + "average.txt");
    write_param(n, stats::variance(results), outprefix + "variance.txt");

    write_distr(n, stats::distribution(results, 100),
                outprefix + "distribution.txt");
}

class direction_redefined : public std::exception {};
class mode_redefined : public std::exception {};

static void set_mode(Mode m)
{
    if (mode != Mode::Undefined && mode != m) {
        throw mode_redefined();
    }
    mode = m;
}

void usage()
{
    std::cerr <<
              "Usage: ./brandes [...args...]\n"
              "-h \t\t show this message\n\n"
              "MODES:\n"
              "-f \t\t load graph from file\n"
              "-b \t\t generate a random binomial graph\n"
              "-e \t\t generate a random evolving graph\n\n"
              "COMMON PARAMETERS:\n"
              "-n X \t\t target number of vertices\n"
              "-d \t\t directed graph\n"
              "-u \t\t undirected graph\n"
              "-o XYZ \t\t outfile prefix\n\n"
              "BINOMIAL GRAPH OPTIONS\n"
              "-p X \t\t probability of a link between vertices\n\n"
              "EVOLVING GRAPH OPTIONS\n"
              "-N X \t\t initial clique size\n"
              "-c X \t\t number of connection a newly created vertex has\n";
}

int main(int argc, char* argv[])
{
    char option;
    std::string user_outprefix;
    while ((option = getopt(argc, argv, "hf:o:ben:N:c:p:du")) != -1) {
        try {
            switch (option) {
            case 'h' :
                usage();
                return 1;
            case 'f' :
                set_mode(Mode::File);
                std::cerr << optarg << std::endl;
                infile = optarg;
                break;
            case 'b' :
                set_mode(Mode::Binomial);
                break;
            case 'e' :
                set_mode(Mode::Evolving);
                break;
            case 'n' :
                genparams.n = std::stoi(optarg);
                break;
            case 'p' :
                genparams.p = std::stod(optarg);
                break;
            case 'd':
                if (genparams.directed != -1) throw direction_redefined();
                genparams.directed = true;
                break;
            case 'u':
                if (genparams.directed != -1) throw direction_redefined();
                genparams.directed = false;
                break;
            case 'N':
                genparams.N = std::stoi(optarg);
                break;
            case 'c':
                genparams.nconnections = std::stoi(optarg);
                break;
            case 'o':
                user_outprefix = optarg;
                user_outprefix += '_';
                break;
            default:
                std::cerr << "Error: Invalid parameter: " << option << "\n";
                exit(EXIT_FAILURE);
            }
        } catch(const Mode&) {
            std::cerr << "Error: Double selection of a mode: " << optarg << "\n";
            exit(EXIT_FAILURE);
        } catch(const direction_redefined&) {
            std::cerr << "Error: Please decide, if you want a directed or"
                      "undirected graph\n";
            exit(EXIT_FAILURE);
        }
    }

    if (mode == Mode::File) {
        // we assume all files use directed
        if (genparams.directed == false) {
            std::cerr << "We do not support undirected files\n";
            return 1;
        }
        genparams.directed = true;
    }

    if (mode == Mode::Undefined) {
        std::cerr << "Error: mode not selected\n";
        return 1;
    } else if (genparams.directed == -1) {
        std::cerr << "Choose either an directed or undirected graph\n";
        return 1;
    }

    if (mode == Mode::Binomial) {
        if (genparams.p < 0 || genparams.p > 1) {
            std::cerr << "Error: invalid p: " << genparams.p << "\n";
            return 1;
        } else if (genparams.n == -1) {
            std::cerr << "Error: invalid number of vertices: " << genparams.n  << "\n";
            return 1;
        } else if (genparams.N != -1 || genparams.nconnections != -1) {
            std::cerr << "BA parameters passed to ER\n";
            return 1;
        }
    } else if (mode == Mode::Evolving) {
        if (genparams.p != -1) {
            std::cerr << "ER parameters passed to BA\n";
            return 1;
        } else if (genparams.N <= 0 || genparams.nconnections <= 0) {
            std::cerr << "Invalid BA parameters\n";
            return 1;
        }
    }
    time_point t0 = std::chrono::high_resolution_clock::now();

    Graph graph;
    if (mode == Mode::File) {
        std::vector<std::pair<int, int>> edges = get_edges(infile);
        graph = Graph(edges);
        char* infile_c = strdup(infile.c_str());
        outprefix = basename(infile_c);
        free(infile_c);
        outprefix += "_";
    } else if (mode == Mode::Binomial) {
        graph = ERGraph(genparams.n, genparams.p, genparams.directed);
        outprefix = "n_" + std::to_string(genparams.n) +
                    "__p_" + std::to_string(genparams.p) + "_";
    } else if (mode == Mode::Evolving) {
        if (genparams.directed) {
            std::cerr << "Sorry, only undirected BA networks supported\n";
            return 1;
        }
        graph = BAGraph(genparams.N, genparams.n, genparams.nconnections);
        outprefix = "n_" + std::to_string(genparams.n) +
                    "__N_" + std::to_string(genparams.N) +
                    "__c_" + std::to_string(genparams.nconnections) + "_";
    } else {
        std::cerr << "Not implemented...\n";
        return 1;
    }
    outprefix = user_outprefix + outprefix;

    std::cout << "Calculating...\n";

    time_point t1 = std::chrono::high_resolution_clock::now();
    std::vector<double> results = brandes(graph);
    time_point t2 = std::chrono::high_resolution_clock::now();

    write_results(results, graph);

    time_point t3 = std::chrono::high_resolution_clock::now();

    std::cerr
            << "Calculation took: "
            << std::chrono::duration_cast<std::chrono::milliseconds>
            (t2-t1).count() << " ms\n"

            << "Including preprocessing: "
            << std::chrono::duration_cast<std::chrono::milliseconds>
            (t3-t0).count() << " ms\n";

    return 0;
}
