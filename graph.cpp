#include "graph.h"

#include <fstream>
#include <sstream>
#include <iostream>

using namespace std;

Graph::Graph(int num_vertices) : edges(num_vertices) {}

Graph::Graph(const vector<std::pair<int, int>>& edges_v)
{
    unordered_map<int, int> vmap;
    auto add_v = [&](int x) {
        if (vmap.find(x) == vmap.end()) {
            vmap.emplace(x, vmap.size());
        }
    };

    for (const auto& p : edges_v) {
        add_v(p.first);
        add_v(p.second);
    }

    int n = vmap.size();
    edges.resize(n);
    for (const auto& p : edges_v) {
        int src = vmap.at(p.first);
        int dest = vmap.at(p.second);
        add_edge(src, dest);
    }
}

void Graph::add_edge(int src, int dest)
{
    edges[src].push_back(dest);
}

void Graph::add_edge_undirected(int src, int dest)
{
    add_edge(src, dest);
    add_edge(dest, src);
}


