#!/bin/bash
INFILE=wiki-vote-sort.txt
OUTFILE=$(mktemp)

rm -f results.txt

for i in $(seq 1 8); do
    echo "Testing with $i threads..."
    ./brandes "$i" "$INFILE" "$OUTFILE" 2>> results.txt
done

awk '{if(NR==1)a=$2;else print $1, a/$2 }' < results.txt > speedup_partial.txt
awk '{if(NR==1)a=$3;else print $1, a/$3 }' < results.txt > speedup.txt
