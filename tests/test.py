#!/usr/bin/env python3
import sys

epsilon = 0.000001

if len(sys.argv) != 3:
    print("Usage: test.py [file-1] [file-2]")
    sys.exit(1)

fname1, fname2 = sys.argv[1], sys.argv[2]
with open(fname1) as f1, open(fname2) as f2:
    for line in zip(f1, f2):
        tokens = list()
        for i in [0, 1]:
            tokens.append(line[i].strip().split())
            assert(len(tokens[i]) == 2)
        v = (tokens[0][0], tokens[1][0])
        bc = (float(tokens[0][1]), float(tokens[1][1]))
        if v[0] != v[1]:
            msg = "Mismatched vertex numbers: {}, {}"
            print(msg.format(v[0], v[1]))
            sys.exit(1)

        if abs(bc[0] - bc[1]) > epsilon:
            msg = "Vertex {}: difference between {} and {} is larger than {}"
            print(msg.format(v[0], bc[0], bc[1], epsilon))
            sys.exit(1)

print("Both outputs are similar!")
