cmake_minimum_required (VERSION 3.1)
project (brandes CXX)

find_package(OpenMP REQUIRED)

message(STATUS "Build type: ${CMAKE_BUILD_TYPE}")

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_FLAGS "-Wall -Wextra -O2 -g -ftree-vectorize ${OpenMP_CXX_FLAGS}")
set(SRC
    main.cpp
    brandes.cpp
    brandes.h
    graph.h
    graph.cpp
    generate_graph.h
    generate_graph.cpp
    stats.h
    stats.cpp
)

add_executable(brandes ${SRC})

install(TARGETS DESTINATION .)
