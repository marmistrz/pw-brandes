#ifndef BRANDES_H
#define BRANDES_H

#include <unordered_map>
#include "graph.h"

//std::map<int, double> brandes_seq(const Graph& graph);
std::vector<double> brandes(const Graph& graph);

#endif // BRANDES_H
