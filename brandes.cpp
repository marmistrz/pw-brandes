#include "brandes.h"

#include <stack>
#include <queue>
#include <mutex>

using namespace std;

template <typename T, typename U>
static void fill_whole(T& container, const U& value)
{
    fill(container.begin(), container.end(), value);
}

vector<double> brandes(const Graph& graph)
{
    int n = graph.edges.size();
    vector<double> res(n);
    double* results = res.data();

    #pragma omp parallel for reduction(+: results[:n])
    for(int s = 0; s < n; ++s) {
        thread_local stack<int> st;
        thread_local vector<vector<int>> p(n);
        thread_local vector<int> sigma(n), d(n);
        thread_local vector<double> delta(n);
        thread_local queue<int> q;

        for(auto& vec : p) {
            vec.clear();
        }
        fill_whole(sigma, 0);
        fill_whole(d, -1);
        fill_whole(delta, 0);

        d[s] = 0;
        sigma[s] = 1;

        q.push(s);

        while (!q.empty()) {
            int v = q.front();
            q.pop();
            st.push(v);

            for (const auto& w : graph.edges[v]) {
                // w found for the first time?
                if (d[w] < 0) {
                    q.push(w);
                    d[w] = d[v] + 1;
                }

                //shortest path to w via v?
                if (d[w] == d[v] + 1) {
                    sigma[w] += sigma[v];
                    p[w].push_back(v);
                }
            }
        }

        while (!st.empty()) {
            int w = st.top();
            st.pop();

            for (int v : p[w]) {
                delta[v] += ((1 + delta[w]) * sigma[v]) / sigma[w];
            }
            if (w != s) {
                results[w] += delta[w];
            }
        }
    }

    return res;
}
